****************************************************
**  DATABASE TABLE PREFIX v1.1 - MS2_changes.txt  **
****************************************************

This is an update to Database Table Prefix 1.0.  This fixes the issue with database backups where a backup of all tables within the database would get backed up rather than just the osCommerce tables.

I believe if you have not defined a prefix, "preg_match" should match all tables, but I haven't bothered to test this out.

This is NOT the Full Package


/admin/backup.php
=================
Look for:

 while ($tables = tep_db_fetch_array($tables_query)) {

          list(,$table) = each($tables);

Add After:

// BOF Database Table Prefix v1.1--------------------------------------------------Insert
         if(preg_match('/^' . DB_TABLE_PREFIX . '/', $table)) {
// EOF Database Table Prefix v1.1--------------------------------------------------------



Look for:

              $schema = ereg_replace(', $', '', $schema) . ');' . "\n";
              fputs($fp, $schema);
            }
          }

Add after:

// BOF Database Table Prefix v1.1--------------------------------------------------Insert
         }
// EOF Database Table Prefix v1.1--------------------------------------------------
