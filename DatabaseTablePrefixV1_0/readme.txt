***********************************************
**  DATABASE TABLE PREFIX v1.0 - readme.txt  **
***********************************************
Author       : Roger Barrett, aka Wizzud
Date         : 17 July 2003
Related Texts: MS1CVS_changes.txt, MS2_changes.txt

I've put this contribution together because I've seen a few related questions, and I know that some hosting packages only give you access to a single database (like mine, for example!).

The object is simple: stick a small common prefix in front of each database table in the catalog, so that you can easily tell one catalog from another within the same database.

I've done this by:
  1. Adding a variable - DB_TABLE_PREFIX - to the configure.php files
  2. Inserting this new variable into all the define statements for the database tables

I have also:
  a. Modified the Backup tool to include the catalog's prefix in the file name and the header within the file
  b. Added a checkbox to the Backup options enabling backup of just the tables matching the current catalog prefix, or the entire database
  c. Modified the New Installation routines to capture and configure the table prefix, and to apply that prefix when loading the standard demo oscommerce shop
  d. Modified the Update Installation routines to cope with applying an update to a catalog that already has a prefix applied to it

I have provided descriptions of the changes required for a standard 2.2MS2 release, and for a CVS release of 2.2MS1. Whether you will find the MS1 or MS2 instructions more applicable depends entirely upon your own version of osCommerce. I've done the best I can!

I have included the changed source files as applied to a standard, unmodified release for 2.2MS2, which can be used as straight replacements if you have a comparable version. I haven't done the same for MS1 so you'll have to follow my instructions.

For MS1, the files involved are:
- includes/configure.php
- includes/application_top.php
- admin/includes/configure.php
- admin/includes/application_top.php
- admin/backup.php
- install/includes/functions/database.php
- install/templates/pages/install.php
- install/templates/pages/install_3.php
- install/templates/pages/install_4.php
- install/templates/pages/install_5.php
- install/templates/pages/upgrade.php
- install/templates/pages/upgrade_3.php

For MS2, the files involved are:
- includes/configure.php
- includes/database_tables.php
- admin/includes/configure.php
- admin/includes/database_tables.php
- admin/backup.php
- install/includes/functions/database.php
- install/templates/pages/install_2.php
- install/templates/pages/install_3.php
- install/templates/pages/install_6.php
- install/templates/pages/install_7.php
- install/templates/pages/upgrade.php
- install/templates/pages/upgrade_3.php

Enjoy!