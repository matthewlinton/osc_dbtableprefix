# OSC_DBTablePrefix

This is an update to Roger Barrett's Prefix for Database Tables.  This fixes the issue with database backups where a backup of all tables within the database would get backed up rather than just the osCommerce tables. I have included version 1.0 of Roger Barrett's Prefix for Database Tables for your convenience.

For more information see http://addons.oscommerce.com/info/1358

## Installation
1. Install version 1.0 DatabaseTablePrefix.
1. Download the latest version (https://bitbucket.org/matthewlinton/osc_dbtableprefix/downloads)
1. Unzip the package.
1. Switch to the "OSC_DBTablePrefix" directory.
1. Follow the directions in "database_table_prefix_1.1.txt.